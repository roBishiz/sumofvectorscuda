#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

#define CUDA_DEBUG

#ifdef CUDA_DEBUG

#define CUDA_CHECK_ERROR(err)           \
if (err != cudaSuccess) {          \
printf("Cuda error: %s\n", cudaGetErrorString(err));    \
printf("Error in file: %s, line: %i\n", __FILE__, __LINE__);  \
}                 \

#else

#define CUDA_CHECK_ERROR(err)

#endif

int n;
char _char;

__global__ void addArraysGPU(int *inputArrayFirst, int *inputArraySecond, int *outputArray, int n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	//�������� �� GPU
	if (index < n) {
		outputArray[index] = inputArrayFirst[index] + inputArraySecond[index];
	}
}

__host__ void addArraysCPU(int *inputArrayFirst, int *inputArraySecond, int *outputArray, int n)
{
	for (int iter = 0; iter < n; iter++)
	{
		outputArray[iter] = inputArrayFirst[iter] + inputArraySecond[iter];
	}
}

int main() {

	std::cout << "Count of : ";
	std::cin >> n;
	std::cout << "Output arrays? (Y/N) : ";
	std::cin >> _char;

	int *inputArrayFirst = new int[n];
	int *inputArraySecond = new int[n];
	int *outputArray = new int[n];

	//���������� ������
	for (int iter = 0; iter < n; iter++) {
		inputArrayFirst[iter] = rand() % 10;
		inputArraySecond[iter] = rand() % 10;
	}

	/*if (_char == 'Y' || _char == 'y') {
		for (int iter = 0; iter < n; iter++) {
			printf("[%d]: %d, %d\n", iter, inputArrayFirst[iter], inputArraySecond[iter]);
		}
	}*/

	int *pointerInputArrayFirst, *pointerInputArraySecond, *pointerOutputArray; //��������� �� ���������� ������

	int size = n * sizeof(int); //���������� ������

	cudaMalloc((void**)&pointerInputArrayFirst, size); //��������� ������
	cudaMalloc((void**)&pointerInputArraySecond, size);
	cudaMalloc((void**)&pointerOutputArray, size);

	cudaMemcpy(pointerInputArrayFirst, inputArrayFirst, size, cudaMemcpyHostToDevice); //����������� �� GPU
	cudaMemcpy(pointerInputArraySecond, inputArraySecond, size, cudaMemcpyHostToDevice);

	dim3 blockSize(1024); //����� ���������� ������
	dim3 gridSize(n / 1024 + 1); //������ � ����������� �����
	printf("gridSize.x = %d, blockSize.x = %d\n", gridSize.x, blockSize.x); //��������� ������ �����

	cudaEvent_t start;
	cudaEvent_t stop;

	//������� event'� ��� ������������� � ������ ������� ������ GPU
	CUDA_CHECK_ERROR(cudaEventCreate(&start));
	CUDA_CHECK_ERROR(cudaEventCreate(&stop));

	//�������� ����� �������� �� GPU
	cudaEventRecord(start, 0);

	addArraysGPU << < gridSize, blockSize >> > (pointerInputArrayFirst, pointerInputArraySecond, pointerOutputArray, n); //����� ����
	cudaDeviceSynchronize(); //�����������
	//������� ����� ���������� ������� �� GPU (� ������������)
	cudaEventRecord(stop, 0);
	float time = 0;
	//��������������� � �������� ��������� ��������
	cudaEventSynchronize(stop);
	//������������ ����� ������ GPU
	cudaEventElapsedTime(&time, start, stop);
	//������� ����� ������� � �������
	printf("GPU compute time: %.0f\n", time);

	cudaMemcpy(outputArray, pointerOutputArray, size, cudaMemcpyDeviceToHost);

	if (_char == 'Y' || _char == 'y') {
		//����� ���������� GPU
		printf("Result Matrix C (GPU) :\n");
		/*for (int iter = 0; iter < n; iter++) {
			printf("%d\n", outputArray[iter]);
		}*/
		for (int iter = 0; iter < 10; iter++) {
			printf("[%d] : %d\n", iter, outputArray[iter]);
		}
		for (int iter = n - 10; iter < n; iter++) {
			printf("[%d] : %d\n", iter, outputArray[iter]);
		}
	}

	//��������� ��������� �������
	for (int iter = 0; iter < n; iter++) {
		outputArray[iter] = 0;
	}

	int startCPU = GetTickCount();
	addArraysCPU(inputArrayFirst, inputArraySecond, outputArray, n);
	//������� ����� ���������� ������� �� CPU (� ������������)
	printf("CPU compute time: %i\n", GetTickCount() - startCPU);

	if (_char == 'Y' || _char == 'y') {
		//����� ���������� CPU
		printf("Result Matrix C (CPU) :\n");
		/*for (int iter = 0; iter < n; iter++) {
			printf("%d\n", outputArray[iter]);
		}*/
		for (int iter = 0; iter < 10; iter++) {
			printf("[%d] : %d\n", iter, outputArray[iter]);
		}
		for (int iter = n - 10; iter < n; iter++) {
			printf("[%d] : %d\n", iter, outputArray[iter]);
		}
	}

	//������������ ������
	cudaFree(pointerInputArrayFirst);
	cudaFree(pointerInputArraySecond);
	cudaFree(outputArray);
	return 0;
}